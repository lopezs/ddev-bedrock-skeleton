# WordPress boilerplate using Bedrock + ddev

## Requirements

* PHP >= 7.1
* DDEV - [https://ddev.readthedocs.io/en/stable/#installation](https://ddev.readthedocs.io/en/stable/#installation)
* Docker - [https://docs.docker.com/install/](https://docs.docker.com/install/)

## Usage

### 1. Create a new project:
```sh
$ git clone git@github.com:lopezs/ddev-bedrock-skeleton.git my-wp-site
$ cd my-wp-site
$ bash init.sh
```

The bash init.sh can do the following:

- modify `.ddev/config.custom.yaml` and set "copied.site.com" as appropriate
- Set up child theme (optional)

### 2. WordPress admin page
Access WordPress admin will be found at `https://my-wp-site.ddev.site/wp/wp-admin/`

## Update/adding Wordpress plugins
Use ddev command `ddev composer` to make sure composer is run inside the container
e.g To update project the after after editing `composer.json` run `ddev composer update`

## Theme development

GeneratePress is the default theme. If a child theme has been chosen then it will be a child of GeneratePress

This DDEV environment comes with npm and gulp built on (i.e node / gulp etc are not required on the host machine!). You just prefix npm commands with `ddev`

### Tasks
To run in the child theme folder (if present). 

`ddev npm install` - install project dependencies

`ddev npm start` - start watching theme folder for changes


## Documentation

DDEV documentation is available at [https://ddev.readthedocs.io/en/stable/](https://ddev.readthedocs.io/en/stable/)

Bedrock documentation is available at [https://roots.io/bedrock/docs/](https://roots.io/bedrock/docs/).

