#!/bin/bash
set -e
function check_ddev_installed() {

  if [ -f ".ddev/config.yaml" ]; then
    echo ""
    echo "Warning: a project already appears to be configured. (\".ddev/config.yaml\" already exists.)"
 
		read -r -p "Proceed anyway? [y/N] " response
	  response=${response,,}    # tolower

	  if [[ ! $response =~ ^(yes|y)$ ]]; then		
	    echo ""
	    echo "Operation cancelled."
	    exit 1
	  fi
  fi
}

PWDTOP=${PWD##*/}
ENVFILE=.env
CUSTOM_DDEV=${PWD}/.ddev/config.custom.yaml
ACF_KEY=
CHILD_THEME=mytheme
PROD_URL=

CMD_NPM=${PWD}/.ddev/commands/web/npm
CMD_NVM=${PWD}/.ddev/commands/web/nvm
DDEV_CUSTOM_ENVIRONMENT=${PWD}/.ddev/docker-compose.environment.yaml

# PLugins to activate
WP_PLUGINS="advanced-custom-fields-pro classic-editor gp-premium"

# Theme to use
WP_THEME="generatepress"

check_ddev_installed

read -e -p "ACF Pro Key  : " -i "${ACF_KEY}" ACF_KEY

read -r -p "Check out a GeneratePress child theme? [y/N] " childtheme_clone
childtheme_clone=${childtheme_clone,,}    # tolower

if [[ $childtheme_clone =~ ^(yes|y)$ ]]; then
  read -e -p "Child theme name  : " -i "${CHILD_THEME}" CHILD_THEME
fi

read -e -p "Production URL (optional, for ddev import-db hook) : " -i "${PROD_URL}" PROD_URL


# Alter Bedrock's .env file. Making a few assumptions here...
rm -f ${ENVFILE}
echo "Creating ${ENVFILE}"
cp example.env ${ENVFILE}

echo "Creating ${DDEV_CUSTOM_ENVIRONMENT}"
cp example.docker-compose.environment.yaml ${DDEV_CUSTOM_ENVIRONMENT}

if [ -f "${ENVFILE}" ]; then
	sed -i '/WP_HOME=/d' ${ENVFILE}
	sed -i '/WP_SITEURL=/d' ${ENVFILE}
	echo "WP_HOME='https://$PWDTOP.ddev.site'" >> ${ENVFILE}
	echo "WP_SITEURL='https://$PWDTOP.ddev.site/wp'" >> ${ENVFILE}
	echo "ACF_PRO_KEY='$ACF_KEY'" >> ${ENVFILE}
else
	echo "Warning: a \"${ENVFILE}\" file could not be found. Your project might not work"
fi

ddev config --project-type=wordpress --php-version=7.4 --docroot=web --composer-version=2
ddev start
ddev composer install

# Add a custom config ddev
if [ ! -f "${CUSTOM_DDEV}" ]; then
  echo "Creating ${CUSTOM_DDEV}"
  cp example.config.custom.yaml ${CUSTOM_DDEV}

  # Update import-db hook placeholders
  if [ ! -z "$PROD_URL" ] ;then
    sed -i "s/copied.site.com/${PROD_URL}/g" ${CUSTOM_DDEV}
  fi
  sed -i "s/my-wp-site/${PWDTOP}/g" ${CUSTOM_DDEV}

fi

## Set up Child theme
if [[ $childtheme_clone =~ ^(yes|y)$ ]]; then

  if [ -f "${DDEV_CUSTOM_ENVIRONMENT}" ];then
    echo "Update DDEV_CUSTOM_ENVIRONMENT to point to child theme"
    sed -i '/- THEME_NAME=/d' ${DDEV_CUSTOM_ENVIRONMENT}
    echo "      - THEME_NAME=${CHILD_THEME}" >> ${DDEV_CUSTOM_ENVIRONMENT}
  fi

	CHILD_THEME_PATH=${PWD}/web/app/themes/${CHILD_THEME}
	CHILD_THEME_FILE=${CHILD_THEME_PATH}/assets/scss/style.scss

	echo ""
	echo "Cloning Child theme"
	git clone git@gitlab.com:lopezs/wp-gulp-lopez.git ${CHILD_THEME_PATH}

	# Modify wpgulp.config.js
		sed -i "s/wp-gulp-lopez.site/$PWDTOP.ddev.site/g" ${CHILD_THEME_PATH}/wpgulp.config.js

	# Update Child theme style.scss
	sed -i "s/Theme-name/${CHILD_THEME}/g" ${CHILD_THEME_FILE}
	sed -i "s/parent-theme-name/generatepress/g" ${CHILD_THEME_FILE}

	# Copy style.scss version to theme root so this script can activate the theme
	cp ${CHILD_THEME_FILE} ${CHILD_THEME_PATH}/style.css

  if [ ! -z "$ACF_KEY" ] ;then
    # Create ACF json sync folder
    mkdir ${CHILD_THEME_PATH}/acf-json
  fi

  # Update placeholder in config.custom.yaml
  echo "custom config working directory"
  sed -i "s/web: \/var\/www\/html\/web/web: \/var\/www\/html\/web\/app\/themes\/${CHILD_THEME}/g" ${CUSTOM_DDEV}

fi

ddev restart

# Install the site and change some defaults
ddev exec wp core install --url=$PWDTOP.ddev.site --title=LopezPress --admin_user=lsadmin --admin_password=password --admin_email=lopez.shackleford@gmail.com
ddev exec 'wp language core install en_GB && \
	wp site switch-language en_GB && \
	wp option update timezone_string "Europe/London" && \
	wp option update date_format "d/m/Y" && \
	wp option update blogdescription "" && \
	wp option update time_format "H:i"'


# If a child theme has been installed then activate it
if [[ $childtheme_clone =~ ^(yes|y)$ ]]; then
  echo "Activating child theme: ${CHILD_THEME}"
  ddev exec wp theme activate ${CHILD_THEME}
  ddev exec npm install
else
	# not installing a child so activate main theme
  if [ ! -z "${WP_THEME}" ]; then
      echo "Activating theme: ${WP_THEME} "
  	ddev exec wp theme activate ${WP_THEME}
	fi
fi

if [ ! -z "${WP_PLUGINS}" ]; then 
  ddev exec wp plugin activate ${WP_PLUGINS}
fi

ddev describe
